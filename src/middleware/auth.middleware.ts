import { NextFunction, Request, Response } from 'express';
import jwt, {JwtPayload} from 'jsonwebtoken';
import raw from "./route.async.wrapper.js";

const { APP_SECRET } = process.env;

export const verifyAuth = raw(async (req:Request, res:Response, next:NextFunction) => {
    try {     
        // check header or url parameters or post parameters for token
        const access_token = req.headers['x-access-token'];
  
        if (!access_token) return res.status(403).json({
            status:'Unauthorized',
            payload: 'No token provided.'
        });
  
        // verifies secret and checks exp
        const decoded = await jwt.verify(access_token as string, APP_SECRET as string)
  
        // if everything is good, save to request for use in other routes
        const {id} = decoded as JwtPayload;
        req.user_id=id;
        next();
  
    } catch (error) {
        return res.status(401).json({
            status:'Unauthorized',
            payload: 'Unauthorized - Failed to authenticate token.'
        });
    }
  }
)