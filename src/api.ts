// require('dotenv').config();
import express, { Express } from 'express';
import morgan from 'morgan';
import log from '@ajar/marker';
import cors from 'cors';
import fs from 'fs-extra';
import { fileURLToPath } from "url";
import path, { dirname } from "path";
import { updateLogFile } from './middleware/logFile.middleware.js'
import { connect_db } from './db/mongoose.connection.js';
import artist_controller from './modules/artist/artist.controller.js';
import song_controller from './modules/song/song.controller.js';
import playlist_controller from './modules/playlist/playlist.controller.js';
import user_controller from './modules/user/user.controller.js';
import auth_controller from './modules/auth/auth.controller.js';
import { printErrorMiddleware, logErrorMiddleware, respondWithErrorMiddleware, urlNotFoundMiddleware } from './middleware/errors.handler.js';
import {verifyAuth} from './middleware/auth.middleware.js';
const __dirname: string = dirname(fileURLToPath(import.meta.url));

const { PORT = 8080, HOST = 'localhost', DB_URI = "mongodb://localhost:27017/crud-demo" } = process.env;


class Api {
  LOG_FILE_PATH: string = path.resolve(__dirname, `./logs/users.logs.log`);
  ERRORS_LOG_FILE_PATH: string = path.resolve(__dirname, `./logs/users.errors.log`);

  constructor() {
    const app = express();
    this.genericMiddleware(app);
    this.useRouters(app);
    this.errorHandling(app);
    this.init(app).catch(console.log);
  }

  genericMiddleware(app: Express) {
    app.use(cors());
    app.use(morgan('dev'));
    fs.ensureFileSync(this.LOG_FILE_PATH);
    app.use(updateLogFile(this.LOG_FILE_PATH));
  }

  useRouters(app:Express){
    app.use('/api/auth', auth_controller);
    app.use('/api/artists',verifyAuth, artist_controller);
    app.use('/api/songs',verifyAuth, song_controller);
    app.use('/api/playlists',verifyAuth, playlist_controller);
    app.use('/api/users',verifyAuth, user_controller);
  }

  errorHandling(app: Express) {
    app.use(urlNotFoundMiddleware);
    app.use(printErrorMiddleware);
    app.use(logErrorMiddleware(this.ERRORS_LOG_FILE_PATH));
    app.use(respondWithErrorMiddleware);

  }

  async init(app: Express) {
    await connect_db(DB_URI as string);
    await app.listen(Number(PORT), HOST as string);
    log.magenta(`api is live on`, ` ✨ ⚡  http://${HOST}:${PORT} ✨ ⚡`);
  }
}

const app = new Api();
