import { model, Document, Schema, Types } from "mongoose";

export interface Artist {
    first_name: string;
    last_name: string;
    songs: string[];
    userSubscribes: string[];
}

export interface Song {
    title: string;
    artist: Schema.Types.ObjectId;
    includePlaylists: string[];
    userLikes: string[];
}

export interface Playlist {
    title: string;
    songs: [Song];
    createdBy: string;
    
}

export interface User {
    user_name: string;
    email: string;
    password: string;
    refresh_token: string,
    favorite_artists: string[],
    favorite_songs: string[],
    created_playlists: string[],

}

export interface ErrorResponse {
    status: number;
    message: string;
    stack?: string;
}
