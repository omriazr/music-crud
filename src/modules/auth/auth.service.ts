import jwt from 'jsonwebtoken';

const { CLIENT_ORIGIN, APP_SECRET, ACCESS_TOKEN_EXPIRATION, REFRESH_TOKEN_EXPIRATION } = process.env;

export const generate_tokens = async (user: any) => {
    const access_token = jwt.sign({ id: user._id }, APP_SECRET as string, {
        expiresIn: ACCESS_TOKEN_EXPIRATION // expires in 1 minute
    })
    const refresh_token = jwt.sign({ id: user._id, profile: JSON.stringify(user) }, APP_SECRET as string, {
        expiresIn: REFRESH_TOKEN_EXPIRATION // expires in 60 days... long-term... 
    })
    return {access_token,refresh_token};
};