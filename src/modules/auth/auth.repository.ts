import user_model from "../user/user.model.js";

export const get = async () => {
    const users = await user_model.find();
    // .select(`first_name
    //         last_name
    //         email
    //         phone`);
    return users;
  };