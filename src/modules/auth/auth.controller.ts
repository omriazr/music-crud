import express from 'express'
import morgan from 'morgan'
import log from '@ajar/marker'
import cors from 'cors'
import bcrypt from 'bcryptjs';
import * as user_repository from '../user/user.repository.js';
import HttpException from "../../exceptions/Http.exception.js";
import jwt, {JwtPayload} from 'jsonwebtoken';
import ms from 'ms';
import * as auth_service from './auth.service.js';
import cookieParser from 'cookie-parser';
import raw from "../../middleware/route.async.wrapper.js";

const { CLIENT_ORIGIN, APP_SECRET, ACCESS_TOKEN_EXPIRATION, REFRESH_TOKEN_EXPIRATION } = process.env;

const router = express.Router();
router.use(express.json());
router.use(cookieParser());

router.post('/login',raw(async (req, res) => {
    log.obj(req.body, 'body');
    const payload = req.body;
    const { email, password } = payload;
    const user = await user_repository.get_user_by_emailDB(email);
    if (!user) throw new HttpException(404, "No user found.");
    const compare_password = await bcrypt.compare(password, user.password);
    if (compare_password) {

        const {access_token, refresh_token} = await auth_service.generate_tokens(user);
        user_repository.save_new_refresh_token(user._id, refresh_token);
        res.cookie('refresh_token', refresh_token, {
            maxAge: ms('60d'), //60 days
            httpOnly: true
        })
        res.status(200).json({
            status: 'you are authenticated',
            access_token,
            payload
        })
    } else {
        res.status(403).json({
            status: 'Unauthorized',
            payload: 'wrong email or password'
        })
    }
}));

router.get('/get-access-token',raw(async (req,res)=> {
    //get refresh_token from client - req.cookies
    const {refresh_token} = req.cookies;
  
    console.log({refresh_token});
  
    if (!refresh_token) return res.status(403).json({
        status:'Unauthorized',
        payload: 'No refresh_token provided.'
    });
  
    try{
      // verifies secret and checks expiration
      const decoded = await jwt.verify(refresh_token, APP_SECRET as string)
      console.log({decoded})
      
      //check user refresh token in DB
      const {id, profile} = decoded as JwtPayload;
  
      const access_token = jwt.sign({ id , some:'other value'}, APP_SECRET as string, {
          expiresIn: ACCESS_TOKEN_EXPIRATION //expires in 1 minute
      })
      res.status(200).json({access_token, profile })
    }catch(err){
        console.log('error: ',err)
        return res.status(401).json({
            status:'Unauthorized',
            payload: 'Unauthorized - Failed to verify refresh_token.'
        });
    }   
  }))

router.post('/register', raw(async (req, res) => {
    log.obj(req.body, 'body');
    const payload = req.body;
    const user = await user_repository.register_userDB(payload);
    res.status(200).json({
        status: 'you are register',
        user
    })
}));

router.post('/logout/:userId', raw(async (req, res) => {
    const user_id = req.params.userId;
    await user_repository.delete_refresh_token(user_id);
    res.clearCookie("refresh_token");
    res.status(200).json({
        status: 'you are logged out'
    })
}));

export default router;