import * as user_repository from './user.repository.js';
import * as song_repository from '../song/song.repository.js';
import HttpException from "../../exceptions/Http.exception.js";
import * as artist_repository from '../artist/artist.repository.js';
import bcrypt from 'bcryptjs';



export const create_user = async (payload:any) => {
  const hashPassword = await bcrypt.hash(payload.password, 8);
  payload.password = hashPassword;
  const user = await user_repository.create_userDB(payload);
  for (const artist_id of payload.favorite_artists) {
    const artist = await artist_repository.get_artist_by_idDB(artist_id);
    artist.userSubscribes.push(user._id);
    await artist.save();
  }
  for (const song_id of payload.favorite_songs) {
    const song = await song_repository.get_song_by_idDB(song_id);
    song.userLikes.push(user._id);
    await song.save();
  }
  return user;
};

export const get_all_users = async () => {
  const users = await user_repository.get_all_usersDB();
  if (!users) throw new HttpException(404, `No users found!`);
  // .select(`first_name
  //         last_name
  //         email
  //         phone`);
  return users;
};
export const get_user_by_id = async (user_id:string) => {
  const user = await user_repository.get_user_by_idDB(user_id);
  if (!user) throw new HttpException(404, "No user found.");
  // .select(`-_id
  //     first_name
  //     last_name
  //     email
  //     phone`);
  return user;
};
export const update_user_by_id = async (user_id:string, payload:any) => {
  const user = await user_repository.update_user_by_idDB(user_id, payload);
  if (!user) throw new HttpException(404, `id: ${user_id} not found`);
  return user;
};

export const delete_user_by_id = async (user_id:string) => {
    const user = await user_repository.get_user_by_idDB(user_id);
    if (!user) throw new HttpException(404, `id: ${user_id} not found`);
    // for (const song_id of user.songs) {
    //     await song_repository.delete_song_by_idDB(song_id);
    //     // await song_model.findByIdAndRemove(song_id);
    // }
    await user_repository.delete_user_by_idDB(user_id);
    return user;
};
