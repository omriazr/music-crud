import user_model from "./user.model.js";
import bcrypt from 'bcryptjs';

export const create_userDB = async (payload:any) => {
    const user = await user_model.create(payload);
    return user;
}

export const register_userDB = async (payload:any) => {
    payload.favorite_artists = [];
    payload.favorite_songs = [];
    payload.created_playlists = [];
    const hashPassword = await bcrypt.hash(payload.password, 8);
    payload.password = hashPassword;
    const user = await user_model.create(payload);
    return user;
}

export const get_all_usersDB = async () => {
    const users = await user_model.find();
    // .select(`first_name
    //         last_name
    //         email
    //         phone`);
    return users;
  };

  export const get_user_by_idDB = async (user_id:string) => {
    const user = await user_model.findById(user_id);
    // .select(`-_id
    //     first_name
    //     last_name
    //     email
    //     phone`);
    return user;
  };

  export const get_user_by_emailDB = async (user_email:string) => {
    const user = await user_model.findOne({'email': user_email});
    // .select(`-_id
    //     first_name
    //     last_name
    //     email
    //     phone`);
    return user;
  };

  export const update_user_by_idDB = async (user_id:string, payload:any) => {
    const user = await user_model.findByIdAndUpdate(user_id, payload, {
      new: true,
      upsert: true,
    });
    return user;
  };

  export const delete_user_by_idDB = async (user_id:string) => {
    const user = await user_model.findByIdAndRemove(user_id);
    // .select(`-_id
    //     first_name
    //     last_name
    //     email
    //     phone`);
    return user;
  };

  export const save_new_refresh_token = async (user_id: string ,refresh_token: string) => {
    const user = await user_model.findByIdAndUpdate(user_id,{refresh_token});
    return user;
  };

  export const delete_refresh_token = async (user_id: string) => {
    const user = await user_model.findById(user_id);
    user.refresh_token = '';
    await user.save();
  };


  