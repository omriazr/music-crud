import mongoose from 'mongoose';
import {User} from '../../types/types.js';


const { Schema, model } = mongoose

export const UserSchema = new Schema<User>({
    user_name: { type : String, required : true },
    email: { type : String, required : true },
    password: { type : String, required : true },
    refresh_token: { type : String, required : false },
    favorite_artists: [{ type: Schema.Types.ObjectId, ref:'artist'}],
    favorite_songs: [{ type: Schema.Types.ObjectId, ref:'song'}],
    created_playlists: [{ type: Schema.Types.ObjectId, ref:'playlist'}],
}, {timestamps:true});
  
export default model('user',UserSchema);