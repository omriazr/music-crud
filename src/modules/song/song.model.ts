import mongoose from 'mongoose';
import {Song} from '../../types/types.js';


const { Schema, model } = mongoose

export const SongSchema = new Schema<Song>({
    title  : { type : String, required : true },
    artist : { type: Schema.Types.ObjectId, ref:'artist'},
    includePlaylists : [{ type: Schema.Types.ObjectId, ref:'playlist'}],
    userLikes : [{ type: Schema.Types.ObjectId, ref:'user'}]
}, {timestamps:true});
  
export default model('song',SongSchema);