/* 
  if there is an error thrown in the DB, asyncMiddleware
  will pass it to next() and express will handle the error */
import raw from "../../middleware/route.async.wrapper.js";
import song_model from "./song.model.js";
import artist_model from "../artist/artist.model.js";
import express from 'express';
//   import { CREATE_song, UPDATE_song, validate_song } from './artist.validate.js';
import { NextFunction, Request, Response } from 'express';
import UrlNotFoundException from "../../exceptions/UrlNotFound.exception.js";
import HttpException from "../../exceptions/Http.exception.js";
import * as song_service from "./song.service.js";



const router = express.Router();
// parse json req.body on post routes
router.use(express.json())


// CREATES A NEW SONG
router.post("/", raw(async (req: Request, res: Response, next: NextFunction) => {
    // const {error, value} = await validate_song(req.body, CREATE_song);
    // const valid_song = await validate_song(req.body, CREATE_song);
    const song = await song_service.create_song(req.body);
    res.status(200).json(song);
}));


// GET ALL SONGS
router.get("/", raw(async (req: Request, res: Response, next: NextFunction) => {
    const songs = await song_service.get_all_songs();
    // .select(`-_id 
    //                                       first_name 
    //                                       last_name 
    //                                       email 
    //                                       phone`);
    res.status(200).json(songs);
})
);

// GETS A SINGLE SONG
router.get("/:id", raw(async (req: Request, res: Response, next: NextFunction) => {
    const song = await song_service.get_song_by_id(req.params.id)
    res.status(200).json(song);
})
);

//GET ALL SONGS OF ARRIST
// router.get("/artist/:id", raw(async (req: Request, res: Response, next: NextFunction) => {
//     const songs = await song_service.get_all_atrist_songs(req.params.id)
//     res.status(200).json(songs);
//     })
// );


// UPDATES A SINGLE SONG
router.put("/:id", raw(async (req: Request, res: Response, next: NextFunction) => {
    // const valid_song = await validate_song(req.body, UPDATE_song);
    const song = await song_service.update_song_by_id(req.params.id, req.body);
    res.status(200).json(song);
})
);


// DELETES A song
router.delete("/:id", raw(async (req: Request, res: Response, next: NextFunction) => {
    const song = await song_service.delete_song_by_id(req.params.id);
    res.status(200).json(song);
})
);

export default router;
