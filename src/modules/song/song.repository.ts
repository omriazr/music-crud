import song_model from "./song.model.js";

export const create_songDB = async (payload: any) => {
    const song = await song_model.create(payload);
    return song ;
};

export const get_all_songsDB = async () => {
    const songs = await song_model.find()
    // .select(`-_id 
    //                                       first_name 
    //                                       last_name 
    //                                       email 
    //                                       phone`);
    return songs;
};

export const get_song_by_idDB = async (song_id: string) => {
    const song = await song_model.findById(song_id);
    // .select(`-_id
    //     first_name
    //     last_name
    //     email
    //     phone`);
    return song;
};

export const update_song_by_idDB = async (song_id: string, payload: any) => {
    const song = await song_model.findByIdAndUpdate(song_id, payload, {
        new: true,
        upsert: true,
    });
    return song;
};

export const delete_song_by_idDB = async (song_id: string) => {
    const song = await song_model.findByIdAndRemove(song_id);
    return song;
};