import * as playlist_service from '../playlist/playlist.service.js';
import HttpException from "../../exceptions/Http.exception.js";
import * as song_repository from './song.repository.js';
import * as artist_repository from '../artist/artist.repository.js';
import * as playlist_repository from '../playlist/playlist.repository.js';
import {Song, Playlist, Artist} from '../../types/types.js';



export const create_song = async (payload: any) => {
    const song = await song_repository.create_songDB(payload);
    const artist = await artist_repository.get_artist_by_idDB(payload.artist);
    artist.songs.push(song._id);
    await artist.save();
    return song;
};

export const get_all_songs = async () => {
    const songs = await song_repository.get_all_songsDB();
    if (!songs) throw new HttpException(404, `No songs found!`);
    return songs;
};
export const get_song_by_id = async (song_id: string) => {
    const song = await song_repository.get_song_by_idDB(song_id);
    if (!song) throw new HttpException(404, "No song found.");
    return song;
};

// export const get_all_atrist_songs = async (artist_id: string) => {
//     const songs = await song_model.findById(artist_id).populate('songs', '-_id title')
//     // .select(`-_id
//     //     first_name
//     //     last_name
//     //     email
//     //     phone`);
//     if (!songs) throw new HttpException(404, "No songs found.");
//     return songs;
// };

export const update_song_by_id = async (song_id: string, payload: any) => {
    const song = await song_repository.update_song_by_idDB(song_id, payload)
    if (!song) throw new HttpException(404, `id: ${song_id} not found`);
    return song;
};


export const delete_song_by_id = async (song_id: string) => {
    const song = await song_repository.get_song_by_idDB(song_id);
    const artist = await artist_repository.get_artist_by_idDB(song.artist);
    if (!song) throw new HttpException(404, `id: ${song_id} not found`);
    artist.songs = artist.songs.filter((songId:string) => {
        return songId != song_id;
    });
    for (const playlist_id of song.includePlaylists) {
        await playlist_service.remove_song_from_playlist(playlist_id, song_id);
    }
    await artist.save();


    await song_repository.delete_song_by_idDB(song_id);
    if (!song) throw new HttpException(404, `id: ${song_id} not found`);
    return song;
};
