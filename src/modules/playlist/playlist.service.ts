import HttpException from "../../exceptions/Http.exception.js";
import * as playlist_repository from './playlist.repository.js';
import * as artist_repository from '../artist/artist.repository.js';
import * as song_repository from '../song/song.repository.js';
import * as user_repository from '../user/user.repository.js';

export const create_playlist = async (payload: any) => {
    const playlist = await playlist_repository.create_playlistDB(payload);
    const user = await user_repository.get_user_by_idDB(payload.createdBy);
    user.created_playlists.push(playlist._id);
    await user.save();
    // if ('songs' in req.body) {
    //     const pending = req.body.songs.map(async (song:any) => {
    //         const singleSong = await song_model.findById(song._id);
    //         singleSong.includePlaylists.push(playlist.id);
    //         await singleSong.save();
    //     })
    //     await Promise.all(pending);
    //     // const song = await song_model.findById(req.body.song);
    //     // playlist.songs.push(song);
    //     // await playlist.save();
    //     res.status(200).json(playlist);
    // }
    // const song = await song_model.findById(payload.song);
    // song.includePlaylists.push(playlist._id);
    // await song.save();
    return playlist;
};

export const get_all_playlists = async () => {
    const playlists = await playlist_repository.get_all_playlistsDB();
    if (!playlists) throw new HttpException(404, `No playlists found!`);
    return playlists;
};
export const get_playlist_by_id = async (playlist_id: string) => {
    const playlist = await playlist_repository.get_playlist_by_idDB(playlist_id);
    if (!playlist) throw new HttpException(404, "No playlist found.");
    return playlist;
};

export const add_song_to_playlist = async (playlist_id: string, song_id:string) => {
    const playlist = await playlist_repository.get_playlist_by_idDB(playlist_id);
    const song = await song_repository.get_song_by_idDB(song_id);

    if (!playlist) throw new HttpException(404, "No playlist found.");
    if (!song) throw new HttpException(404, "No song found");

    if(!playlist.songs.includes(song)){
        playlist.songs.push(song)
        await playlist.save();
    }
    if(!song.includePlaylists.includes(playlist_id)){
        song.includePlaylists.push(playlist_id)
        await song.save()
    }
    return playlist;
};

export const remove_song_from_playlist = async (playlist_id: string, song_id:string) => {
    const playlist = await playlist_repository.get_playlist_by_idDB(playlist_id);
    const song = await song_repository.get_song_by_idDB(song_id);

    if (!playlist) throw new HttpException(404, "No playlist found.");
    if (!song) throw new HttpException(404, "No song found");

    playlist.songs = playlist.songs.filter((song:any) => song._id.toString() != song_id);
    song.includePlaylists = song.includePlaylists.filter((playlistId:string) => playlistId != playlist_id);

    await playlist.save();
    await song.save();
    return playlist;
};


export const update_playlist_by_id = async (playlist_id: string, payload: any) => {
    const playlist = await playlist_repository.update_playlist_by_idDB(playlist_id, payload);
    if (!playlist) throw new HttpException(404, `id: ${playlist_id} not found`);
    return playlist;
    
};


export const delete_playlist_by_id = async (playlist_id: string) => {
    const playlist = await playlist_repository.get_playlist_by_idDB(playlist_id);
    if (!playlist) throw new HttpException(404, `id: ${playlist_id} not found`);
    for (const playlist_song of playlist.songs) {
        const song = await song_repository.get_song_by_idDB(playlist_song._id);
        song.includePlaylists = song.includePlaylists.filter((playlistId:string) => playlistId != playlist_id);
        await song.save();
    }
    await playlist_repository.delete_playlist_by_idDB(playlist_id);
    return playlist;
};
