/* 
  if there is an error thrown in the DB, asyncMiddleware
  will pass it to next() and express will handle the error */
import raw from "../../middleware/route.async.wrapper.js";
import song_model from "../song/song.model.js";
import artist_model from "../artist/artist.model.js";
import playlist_model from "./playlist.model.js";
import express from 'express';
//   import { CREATE_playlist, UPDATE_playlist, validate_playlist } from './artist.validate.js';
import { NextFunction, Request, Response } from 'express';
import UrlNotFoundException from "../../exceptions/UrlNotFound.exception.js";
import HttpException from "../../exceptions/Http.exception.js";
import * as playlist_service from "./playlist.service.js";


const router = express.Router();
// parse json req.body on post routes
router.use(express.json())


// CREATES A NEW playlist
router.post("/", raw(async (req: Request, res: Response, next: NextFunction) => {
    // const {error, value} = await validate_playlist(req.body, CREATE_playlist);
    // const valid_playlist = await validate_playlist(req.body, CREATE_playlist);
    const playlist = await playlist_service.create_playlist(req.body);
    res.status(200).json(playlist);
}));


// GET ALL playlistS
router.get("/", raw(async (req: Request, res: Response, next: NextFunction) => {
    const playlists = await playlist_service.get_all_playlists();
    // .select(`-_id 
    //                                       first_name 
    //                                       last_name 
    //                                       email 
    //                                       phone`);
    res.status(200).json(playlists);
})
);

// GETS A SINGLE playlist
router.get("/:id", raw(async (req: Request, res: Response, next: NextFunction) => {
    const playlist = await playlist_service.get_playlist_by_id(req.params.id);
    res.status(200).json(playlist);
})
);

//localhost:8080/api/playlists/61dc466a83b5b56629057c2b/song/61dc44b183b5b56629057c27
//ADD SONG TO PLAYLIST
router.post("/:playlist_id/song/:song_id", raw(async (req: Request, res: Response, next: NextFunction) => {
    const { playlist_id, song_id } = req.params;
    const playlist = await playlist_service.add_song_to_playlist(playlist_id, song_id);
    res.status(200).json(playlist);
})
);

router.delete("/:playlist_id/song/:song_id", raw(async (req: Request, res: Response, next: NextFunction) => {
    const { playlist_id, song_id } = req.params;
    const playlist = await playlist_service.remove_song_from_playlist(playlist_id, song_id);
    res.status(200).json(playlist);
})
);


// UPDATES A SINGLE playlist
router.put("/:id", raw(async (req: Request, res: Response, next: NextFunction) => {
    // const valid_playlist = await validate_playlist(req.body, UPDATE_playlist);
    const playlist = await playlist_service.update_playlist_by_id(req.params.id, req.body);
    res.status(200).json(playlist);
})
);


// DELETES A playlist
router.delete("/:id", raw(async (req: Request, res: Response, next: NextFunction) => {
    const playlist = await playlist_service.delete_playlist_by_id(req.params.id);
    if (!playlist) {
        return next(new UrlNotFoundException(req.originalUrl));
    }
    res.status(200).json(playlist);
})
);

export default router;
