import playlist_model from "./playlist.model.js";

export const create_playlistDB = async (payload: any) => {
    const playlist = await playlist_model.create(payload);
    return playlist;
};

export const get_all_playlistsDB = async () => {
    const playlists = await playlist_model.find()
    // .select(`-_id 
    //                                       first_name 
    //                                       last_name 
    //                                       email 
    //                                       phone`);
    return playlists;
};


export const get_playlist_by_idDB = async (playlist_id: string) => {
    const playlist = await playlist_model.findById(playlist_id);
    // .select(`-_id
    //     first_name
    //     last_name
    //     email
    //     phone`);
    return playlist;
};

export const update_playlist_by_idDB = async (playlist_id: string, payload: any) => {
    const playlist = await playlist_model.findByIdAndUpdate(playlist_id, payload, {
        new: true,
        upsert: true,
    });
    return playlist;
};

export const delete_playlist_by_idDB = async (playlist_id: string) => {
    const playlist = await playlist_model.findByIdAndRemove(playlist_id);
    return playlist;
};