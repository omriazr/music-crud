import { number } from 'joi';
import mongoose from 'mongoose';
import { SongSchema } from '../song/song.model.js';
import {Playlist} from '../../types/types.js';

const { Schema, model } = mongoose;

export const PlaylistSchema = new Schema<Playlist>({
    title  : { type : String, required : true },
    songs : [SongSchema],
    createdBy: { type: Schema.Types.ObjectId, ref:'user'}
}, {timestamps:true});
  
export default model('playlist',PlaylistSchema);