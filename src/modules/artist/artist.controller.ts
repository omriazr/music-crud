/* 
  if there is an error thrown in the DB, asyncMiddleware
  will pass it to next() and express will handle the error */
import raw from "../../middleware/route.async.wrapper.js";
import artist_model from "./artist.model.js";
import express from 'express';
// import { CREATE_USER, UPDATE_USER, validate_user } from './artist.validate.js';
import { NextFunction, Request, Response } from 'express';
import UrlNotFoundException from "../../exceptions/UrlNotFound.exception.js";
import HttpException from "../../exceptions/Http.exception.js";
import * as artist_service from "./artist.service.js";



const router = express.Router();

// parse json req.body on post routes
router.use(express.json())


// CREATES A NEW USER
router.post("/", raw(async (req: Request, res: Response, next:NextFunction) => {
  // const valid_user = await validate_user(req.body, CREATE_USER);
  const user = await artist_service.create_artist(req.body);
  res.status(200).json(user);
}));



// GET ALL USERS
router.get("/", raw(async (req: Request, res: Response, next:NextFunction) => {
  const users = await artist_service.get_all_artists();
  if (!users) return next(new HttpException(404, `No users found!`));
  res.status(200).json(users);
})
);

// GETS A SINGLE USER
router.get("/:id", raw(async (req: Request, res: Response, next: NextFunction) => {
  const user = await artist_service.delete_artist_by_id(req.params.id);
  if (!user) return next(new HttpException(404, "No user found." ));
  res.status(200).json(user);
})
);
// UPDATES A SINGLE USER
router.put("/:id", raw(async (req: Request, res: Response, next: NextFunction) => {
  const user = await artist_service.update_artist_by_id(req.params.id, req.body);
  if (!user) return next(new HttpException(404, `id: ${req.params.id} not found` ));
  res.status(200).json(user);
})
);


// DELETES A USER
router.delete("/:id", raw(async (req: Request, res: Response, next: NextFunction) => {
  const user = await artist_service.delete_artist_by_id(req.params.id);
  if (!user) {
    return next(new UrlNotFoundException(req.originalUrl));
  }
  res.status(200).json(user);
})
);

export default router;
