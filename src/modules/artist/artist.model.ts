import mongoose from 'mongoose';
import {Artist} from '../../types/types.js';

const { Schema, model } = mongoose;

const ArtistSchema = new Schema<Artist>({
    first_name  : {type : String, required: true},
    last_name   : {type : String, required: true},
    songs    : [{ type: Schema.Types.ObjectId, ref:'song'}],
    userSubscribes: [{ type: Schema.Types.ObjectId, ref:'user'}]
}, {timestamps:true});
  
export default model('artist',ArtistSchema);