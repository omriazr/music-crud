import * as artist_repository from './artist.repository.js';
import * as song_repository from '../song/song.repository.js';
import HttpException from "../../exceptions/Http.exception.js";

export const create_artist = async (payload:any) => {
  const artist = await artist_repository.create_artistDB(payload);
  return artist;
};
export const get_all_artists = async () => {
  const artists = await artist_repository.get_all_artistsDB();
  if (!artists) throw new HttpException(404, `No artists found!`);
  // .select(`first_name
  //         last_name
  //         email
  //         phone`);
  return artists;
};
export const get_artist_by_id = async (artist_id:string) => {
  const artist = await artist_repository.get_artist_by_idDB(artist_id);
  if (!artist) throw new HttpException(404, "No artist found.");
  // .select(`-_id
  //     first_name
  //     last_name
  //     email
  //     phone`);
  return artist;
};
export const update_artist_by_id = async (artist_id:string, payload:any) => {
  const artist = await artist_repository.update_artist_by_idDB(artist_id, payload);
  if (!artist) throw new HttpException(404, `id: ${artist_id} not found`);
  return artist;
};

export const delete_artist_by_id = async (artist_id:string) => {
    const artist = await artist_repository.get_artist_by_idDB(artist_id);
    if (!artist) throw new HttpException(404, `id: ${artist_id} not found`);
    for (const song_id of artist.songs) {
        await song_repository.delete_song_by_idDB(song_id);
        // await song_model.findByIdAndRemove(song_id);
    }
    await artist_repository.delete_artist_by_idDB(artist_id);
    return artist;
};
